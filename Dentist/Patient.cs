﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dentist
{
    public class Patient
    {
        public int Number { get; set; }
        public int Arrival { get; set; }
        public int Duration { get; set; }
    }
}
