﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dentist
{
    public class PickerV2 : Picker
    {

        public PickerV2()
        {

        }
        public string GeefNamen()
        {
            return "Jay Jay Spitse";
        }

        public Patient selectPatient(List<Patient> room, int clock)
        {
            int shortestDuration = 0;
            int longestDuration = 0;
            int longestWaittime = 0;
            Patient finalpatient = null;

            foreach (Patient timePatient in room) 
            {
                if (timePatient.Duration > longestDuration)
                {
                    longestDuration = timePatient.Duration;
                }
            }

            foreach (Patient patient in room)
            {
                int currentWaitTime = clock - patient.Arrival;

                if (patient.Duration < shortestDuration || shortestDuration == 0)
                {
                    shortestDuration = patient.Duration;
                    finalpatient = patient;
                }

                int newWaitTime = currentWaitTime + longestDuration;

                if (newWaitTime > longestWaittime && newWaitTime >= 10) 
                {
                    longestWaittime = newWaitTime;
                    finalpatient = patient;
                }        
            }
            return finalpatient;
        }
    }
}
